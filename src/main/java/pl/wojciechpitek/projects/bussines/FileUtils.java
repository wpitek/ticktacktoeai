package pl.wojciechpitek.projects.bussines;

import pl.wojciechpitek.projects.enums.TypeEnum;
import pl.wojciechpitek.projects.holders.DataHolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class FileUtils {

    private static DataHolder dataHolder = DataHolder.getInstance();

    public static void readDataFromFile(String filename) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filename));
        Integer counter = 0;
        String line = br.readLine();
        while (line != null) {
            try {
                if (counter++ < 1) {
                    dataHolder.setRoundNumber(Integer.parseInt(line));
                } else {
                    String[] parts = line.split(",");
                    Integer type = Integer.parseInt(parts[0].trim()),
                        x = Integer.parseInt(parts[1].trim()),
                        y = Integer.parseInt(parts[2].trim());
                    dataHolder.addDataToMap(x, y, TypeEnum.getTypeFromInteger(type));
                }
                line = br.readLine();
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                throw new IllegalArgumentException("Bad data consistency!");
            }
        }
    }

    public static void saveDataToFile(String filename, Integer nextMove) throws IOException {
        TypeEnum type = dataHolder.getRoundNumber() % 2 == 0 ? TypeEnum.CIRCLE : TypeEnum.CROSS;
        File file = new File(filename);
        List<String> lines = Files.readAllLines(file.toPath());
        Integer roundNumber = dataHolder.getRoundNumber() + 1;
        lines.set(0, roundNumber.toString());
        lines.add(String.format("%d,%d,%d", type.getType(), nextMove % 10, nextMove / 10));
        Files.write(file.toPath(), lines);
    }
}
