package pl.wojciechpitek.projects.enums;

import java.awt.*;

public enum TypeEnum {
    CIRCLE(0, Color.GREEN, -1), CROSS(1, Color.RED, 1);

    private Integer type;
    private Color color;
    private Integer minimax;

    TypeEnum(Integer type, Color color, Integer minimax) {
        this.type = type;
        this.color = color;
        this.minimax = minimax;
    }

    public Color getColor() {
        return color;
    }

    public Integer getType() {
        return type;
    }

    public Integer getMinimax() {
        return minimax;
    }

    public static TypeEnum getTypeFromInteger(Integer type) {
        switch (type) {
            case 0:
                return CIRCLE;
            case 1:
                return CROSS;
            default:
                return null;
        }
    }
}
