package pl.wojciechpitek.projects.holders;

import pl.wojciechpitek.projects.enums.TypeEnum;

import java.util.HashMap;
import java.util.Map;

public class DataHolder {
    private Integer roundNumber = 0;
    private Map<Integer, Map<Integer, TypeEnum>> data = new HashMap<>();

    private DataHolder() {
        for(int i = 0; i < 9; i++) {
            data.put(i, new HashMap<>());
        }
    }

    public Integer getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(Integer roundNumber) {
        this.roundNumber = roundNumber;
    }

    public Map<Integer, Map<Integer, TypeEnum>> getData() {
        return data;
    }

    public void addDataToMap(Integer x, Integer y, TypeEnum type) {
        if (!canAddData(x, y)) {
            throw new IllegalArgumentException("Key already exists!");
        }
        data.get(x).put(y, type);
    }

    public TypeEnum getItemOnPosition(Integer x, Integer y) {
        if (!canAddData(x, y)) {
            return data.get(x).get(y);
        }
        return null;
    }

    public static DataHolder getInstance() { return Holder.INSTANCE; }

    private static class Holder {
        static final DataHolder INSTANCE = new DataHolder();
    }

    private Boolean canAddData(Integer x, Integer y) {
        return !(data.containsKey(x) && data.get(x).containsKey(y));
    }
}
