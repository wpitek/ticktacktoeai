package pl.wojciechpitek.projects.frames;

import pl.wojciechpitek.projects.bussines.ArtificialIntelligence;
import pl.wojciechpitek.projects.bussines.FileUtils;
import pl.wojciechpitek.projects.enums.TypeEnum;
import pl.wojciechpitek.projects.holders.DataHolder;
import pl.wojciechpitek.projects.panels.Cell;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFrame extends JFrame {

    private List<Cell> cells = new ArrayList<>();
    private DataHolder data = DataHolder.getInstance();

    public MainFrame(String fileName) throws IOException{
        FileUtils.readDataFromFile(fileName);
        ArtificialIntelligence ai = new ArtificialIntelligence();
        checkWinner(ai);
        Integer nextMove = ai.doTheMove();
        checkWinner(ai);
        initUi(nextMove - 11);
        FileUtils.saveDataToFile(fileName, nextMove);
    }

    private void checkWinner(ArtificialIntelligence ai) {
        TypeEnum won = ai.isWon();
        if (won != null) {
            JOptionPane.showMessageDialog(this, "Zwyciezyl gracz: " + (TypeEnum.CROSS.equals(won) ? "krzyzyk" : "kolko"));
        }
    }

    private void initUi(Integer move) {
        setTitle("tic tac toe - Wojciech Pitek");
        setSize(new Dimension(600, 600));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new GridLayout(10, 10, 2, 2));

        for(int i = 0; i < 100; i++) {
            Cell cell = new Cell();
            TypeEnum te = data.getItemOnPosition(i % 10 + 1, i / 10 + 1);
            if (move != null && move.equals(i)) {
                cell.setBackground(Color.BLUE);
            }
            if (te != null) {
                cell.setBackground(te.getColor());
            }
            cells.add(cell);
            add(cell);
        }
    }

}
