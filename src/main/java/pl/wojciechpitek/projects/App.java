package pl.wojciechpitek.projects;

import pl.wojciechpitek.projects.frames.MainFrame;

import java.awt.*;
import java.io.IOException;

public class App {

    public static void main(String args[]) {
//        for (int i = 4; i <= 9; i++) {
//            for (int j = 0; j <= 5; j++) {
//                System.out.print("{");
//                for (int k = 0; k < 5; k++) {
//                    System.out.print(10 * i - 10 * k + j + k + (k == 4 ? "" : ", "));
//                }
//                System.out.print("}, ");
//            }
//            System.out.println("");
//        }
        EventQueue.invokeLater(() -> {
            MainFrame panel = null;
            try {
                String fileName;
                try {
                    fileName = args[0];
                } catch (ArrayIndexOutOfBoundsException e) {
                    fileName = "C:\\Users\\Wojciech\\data";
                }
                panel = new MainFrame(fileName);
                panel.setVisible(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
